<?php

namespace App\Http\ApiV1\Support\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseFormRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [];
    }

    public static function nestedRules(string $prefix, array $rules): array
    {
        return collect($rules)->mapWithKeys(function ($rule, $key) use ($prefix) {
            return ["{$prefix}.{$key}" => $rule];
        })->all();
    }
}
