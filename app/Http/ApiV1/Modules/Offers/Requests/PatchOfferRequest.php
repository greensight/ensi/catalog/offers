<?php

namespace App\Http\ApiV1\Modules\Offers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchOfferRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'allow_publish' => ['sometimes', 'boolean'],
            'price' => ['sometimes', 'nullable', 'integer', 'min:0'],
        ];
    }
}
