<?php

namespace App\Http\ApiV1\Modules\Offers\Queries;

use App\Domain\Offers\Models\Offer;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OffersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Offer::query());

        $this->allowedIncludes(['stocks']);
        $this->allowedSorts(['id', 'product_id', 'price', 'allow_publish', 'is_active', 'created_at', 'updated_at']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('allow_publish'),
            AllowedFilter::exact('is_active'),
            ...NumericFilter::make('price')->lte()->gte(),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
