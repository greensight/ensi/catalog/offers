<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\Modules\Stocks\Resources\StocksResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Offer
 */
class OffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'price' => $this->price,
            'allow_publish' => $this->allow_publish,
            'is_active' => $this->is_active,
            'is_real_active' => $this->isRealActive(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'stocks' => StocksResource::collection($this->whenLoaded('stocks')),
        ];
    }
}
