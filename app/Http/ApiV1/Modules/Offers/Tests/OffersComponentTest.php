<?php

use App\Domain\Offers\Jobs\MigrateEntitiesFromCatalogJob;
use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\Modules\Offers\Tests\Factories\OfferRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('offers', 'component');

test('PATCH /api/v1/offers/offers', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $offer = Offer::factory()->create();

    $request = OfferRequestFactory::new()->make();

    patchJson("/api/v1/offers/offers/{$offer->id}", $request)
        ->assertOk()
        ->assertJsonPath('data.price', $request['price']);

    assertDatabaseHas(
        $offer->getTable(),
        ['id' => $offer->id, 'price' => $request['price']]
    );
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/offers/offers/{id} success', function () {
    $offer = Offer::factory()->create();

    getJson("/api/v1/offers/offers/{$offer->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $offer->id);
});

test('GET /api/v1/offers/offers/{id} 404', function () {
    getJson('/api/v1/offers/offers/404')
        ->assertStatus(404);
});

test('POST /api/v1/offers/offers:search 200', function () {
    $offers = Offer::factory()
        ->count(10)
        ->sequence(
            ['allow_publish' => false],
            ['allow_publish' => true],
        )
        ->create();

    postJson('/api/v1/offers/offers:search', [
        "filter" => ["allow_publish" => true],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $offers->last()->id)
        ->assertJsonPath('data.0.allow_publish', true);
});

test("POST /api/v1/offers/offers:search filter success", function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    mixed $filterValue,
) {
    /** @var Offer $offer */
    $offer = Offer::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/offers/offers:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $offer->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $offer->id);
})->with([
    ['id', null, null, null],
    ['product_id', 10, null, [10, 11]],
    ['allow_publish', true, null, null],
    ['is_active', true, null, null],
    ['price', 100, 'price_gte', 99],
    ['price', 100, 'price_lte', 101],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/offers/offers:search sort success", function (string $sort) {
    Offer::factory()->create();
    postJson("/api/v1/offers/offers:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'product_id', 'price', 'allow_publish', 'is_active', 'created_at', 'updated_at',
]);

test('POST /api/v1/offers/offers:migrate 200', function () {
    Queue::fake();

    postJson('/api/v1/offers/offers:migrate')->assertOk();

    Queue::assertPushed(MigrateEntitiesFromCatalogJob::class);
});
