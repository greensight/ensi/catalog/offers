<?php

namespace App\Http\ApiV1\Modules\Offers\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class OfferRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'allow_publish' => $this->faker->boolean(),
            'price' => $this->faker->nullable()->numberBetween(10, 100_000),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
