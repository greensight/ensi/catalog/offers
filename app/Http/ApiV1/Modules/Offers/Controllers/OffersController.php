<?php

namespace App\Http\ApiV1\Modules\Offers\Controllers;

use App\Domain\Offers\Actions\Offers\PatchOfferAction;
use App\Domain\Offers\Jobs\MigrateEntitiesFromCatalogJob;
use App\Http\ApiV1\Modules\Offers\Queries\OffersQuery;
use App\Http\ApiV1\Modules\Offers\Requests\PatchOfferRequest;
use App\Http\ApiV1\Modules\Offers\Resources\OffersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class OffersController
{
    public function search(PageBuilderFactory $pageBuilderFactory, OffersQuery $query): Responsable
    {
        return OffersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, OffersQuery $query): Responsable
    {
        return new OffersResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchOfferRequest $request, PatchOfferAction $action): Responsable
    {
        return new OffersResource($action->execute($id, $request->validated()));
    }

    public function migrate(): Responsable
    {
        MigrateEntitiesFromCatalogJob::dispatch();

        return new EmptyResource();
    }
}
