<?php

namespace App\Http\ApiV1\Modules\Stocks\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStockRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'qty' => ['sometimes', 'nullable', 'numeric', 'min:0'],
        ];
    }
}
