<?php

namespace App\Http\ApiV1\Modules\Stocks\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReserveStocksRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'items' => ['required', 'array'],
            'items.*.store_id' => ['required', 'integer'],
            'items.*.offer_id' => ['required', 'integer'],
            'items.*.qty' => ['required', 'numeric', 'min:0'],
        ];
    }

    public function getItems(): array
    {
        return $this->get('items');
    }
}
