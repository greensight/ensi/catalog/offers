<?php

use App\Domain\Stocks\Models\Stock;
use App\Http\ApiV1\Modules\Stocks\Tests\Factories\StockRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/stocks/stocks/{id} 200', function () {
    $stock = Stock::factory()->create();

    getJson("/api/v1/stocks/stocks/{$stock->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $stock->id);
});

test('GET /api/v1/stocks/stocks/{id} 404', function () {
    getJson('/api/v1/stocks/stocks/404')
        ->assertStatus(404);
});

test('PATCH /api/v1/stocks/stocks/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $stock = Stock::factory()->create();

    $request = StockRequestFactory::new()->make();

    patchJson("/api/v1/stocks/stocks/{$stock->id}", $request)
        ->assertOk();

    assertDatabaseHas(
        Stock::class,
        ['id' => $stock->id, 'qty' => $request['qty']]
    );
})->with([true]);

test("POST /api/v1/stocks/stocks:search filter success", function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    mixed $filterValue,
) {
    /** @var Stock $stock */
    $stock = Stock::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/stocks/stocks:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $stock->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $stock->id);
})->with([
    ['id', null, null, null],
    ['store_id', null, null, null],
    ['offer_id', null, null, null],
    ['qty', 100, 'qty_gte', 99.99],
    ['qty', 100, 'qty_lte', 100.01],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/stocks/stocks:search sort success", function (string $sort) {
    Stock::factory()->create();
    postJson("/api/v1/stocks/stocks:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'store_id', 'offer_id', 'qty', 'created_at', 'updated_at',
]);
