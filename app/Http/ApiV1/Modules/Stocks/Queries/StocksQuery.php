<?php

namespace App\Http\ApiV1\Modules\Stocks\Queries;

use App\Domain\Stocks\Models\Stock;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StocksQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Stock::query());

        $this->allowedSorts([
            'id',
            'store_id',
            'offer_id',
            'qty',
            'created_at',
            'updated_at',
        ]);

        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('store_id'),
            AllowedFilter::exact('offer_id'),
            ...NumericFilter::make('qty')->lte()->gte(),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
