<?php

namespace App\Http\ApiV1\Modules\Stocks\Controllers;

use App\Domain\Stocks\Actions\Stocks\PatchStockAction;
use App\Domain\Stocks\Actions\Stocks\ReserveStockAction;
use App\Http\ApiV1\Modules\Stocks\Queries\StocksQuery;
use App\Http\ApiV1\Modules\Stocks\Requests\PatchStockRequest;
use App\Http\ApiV1\Modules\Stocks\Requests\ReserveStocksRequest;
use App\Http\ApiV1\Modules\Stocks\Resources\StocksResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StocksController
{
    public function search(PageBuilderFactory $pageBuilderFactory, StocksQuery $query): Responsable
    {
        return StocksResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, StocksQuery $query): Responsable
    {
        return new StocksResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchStockRequest $request, PatchStockAction $action): Responsable
    {
        return new StocksResource($action->execute($id, $request->validated()));
    }

    public function reserve(ReserveStocksRequest $request, ReserveStockAction $action): Responsable
    {
        $action->execute($request->getItems());

        return new EmptyResource();
    }
}
