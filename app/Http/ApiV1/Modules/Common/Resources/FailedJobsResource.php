<?php

namespace App\Http\ApiV1\Modules\Common\Resources;

use App\Domain\Common\Models\FailedJob;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin FailedJob
 */
class FailedJobsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'uuid' => $this->uuid,
            'connection' => $this->connection,
            'queue' => $this->queue,
            'payload' => $this->payload,
            'exception' => $this->exception,

            'failed_at' => $this->failed_at,
        ];
    }
}
