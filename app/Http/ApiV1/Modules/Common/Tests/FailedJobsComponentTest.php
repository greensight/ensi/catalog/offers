<?php

use App\Domain\Common\Models\FailedJob;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/common/failed-jobs:search 200', function () {
    $models = FailedJob::factory()
        ->count(5)
        ->create();

    postJson('/api/v1/common/failed-jobs:search', [
        'sort' => ['-id'],
    ])
        ->assertStatus(200)
        ->assertJsonCount($models->count(), 'data')
        ->assertJsonPath('data.0.id', $models->last()->id);
});

test('POST /api/v1/common/failed-jobs:search filter success', function (
    string $fieldKey,
    mixed $value = null,
    ?string $filterKey = null,
    mixed $filterValue = null,
) {
    /** @var FailedJob $failedJob */
    $failedJob = FailedJob::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/common/failed-jobs:search', ['filter' => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $failedJob->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $failedJob->id);
})->with([
    ['id'],
    ['uuid'],
    ['connection'],
    ['queue'],
    ['payload', '{"uuid": "01d40bf2-4a03-4637-9b74-f4ed2e0c2751"}', 'payload_like', '01d40bf2-4a03-4637-9b74-f4ed2e0c2751'],
    ['exception', 'Redis connection failed', 'exception_like', 'redis'],
    ['failed_at', '2022-04-20', 'failed_at_gte', '2022-04-19'],
    ['failed_at', '2022-04-20', 'failed_at_lte', '2022-04-21'],
]);

test('POST /api/v1/common/failed-jobs:search sort success', function (string $sort) {
    FailedJob::factory()->create();
    postJson('/api/v1/common/failed-jobs:search', ['sort' => [$sort]])->assertOk();
})->with([
    'id', 'connection', 'queue', 'failed_at',
]);
