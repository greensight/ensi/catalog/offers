<?php

use App\Domain\Common\Jobs\SyncEntitiesJob;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/common/entities:sync 200', function () {
    Queue::fake();

    postJson('/api/v1/common/entities:sync')->assertOk();

    Queue::assertPushed(SyncEntitiesJob::class);
});
