<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Http\ApiV1\Modules\Common\Queries\FailedJobsQuery;
use App\Http\ApiV1\Modules\Common\Resources\FailedJobsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class FailedJobsController
{
    public function search(FailedJobsQuery $query, PageBuilderFactory $builderFactory): Responsable
    {
        return FailedJobsResource::collectPage(
            $builderFactory->fromQuery($query)->build()
        );
    }
}
