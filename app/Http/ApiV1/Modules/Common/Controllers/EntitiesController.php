<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Domain\Common\Jobs\SyncEntitiesJob;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class EntitiesController
{
    public function sync(): Responsable
    {
        SyncEntitiesJob::dispatch();

        return new EmptyResource();
    }
}
