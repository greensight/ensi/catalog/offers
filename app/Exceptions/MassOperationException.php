<?php

namespace App\Exceptions;

use RuntimeException;

class MassOperationException extends RuntimeException
{
    public function __construct(protected array $invalidTargets, ?string $message = null)
    {
        parent::__construct($message ?? 'Не все объекты были обработаны', 500);
    }

    public function getInvalidTargets(): array
    {
        return $this->invalidTargets;
    }
}
