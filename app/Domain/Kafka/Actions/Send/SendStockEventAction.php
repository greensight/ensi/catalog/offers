<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\StockPayload;
use App\Domain\Stocks\Models\Stock;

class SendStockEventAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(Stock $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new StockPayload($model), $event, 'stocks');
        $this->sendAction->execute($modelEvent);
    }
}
