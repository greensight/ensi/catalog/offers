<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\OfferPayload;
use App\Domain\Offers\Models\Offer;

class SendOfferEventAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(Offer $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new OfferPayload($model), $event, 'offers');
        $this->sendAction->execute($modelEvent);
    }
}
