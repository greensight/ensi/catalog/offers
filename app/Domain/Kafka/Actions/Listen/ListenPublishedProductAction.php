<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct\PublishedProductEventMessage;
use App\Domain\Offers\Actions\Offers\AutoCreateBaseOfferAction;
use App\Domain\Offers\Actions\Offers\CalculationOffersActivityAction;
use App\Domain\Offers\Actions\Offers\Data\CalculationOfferContext;
use App\Domain\Offers\Actions\Offers\Data\ProductData;
use RdKafka\Message;

class ListenPublishedProductAction
{
    public function __construct(
        protected readonly AutoCreateBaseOfferAction $autoCreateOfferAction,
        protected readonly CalculationOffersActivityAction $calculationOfferActivityAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = PublishedProductEventMessage::makeFromRdKafka($message);
        $product = $eventMessage->attributes;

        if ($eventMessage->isCreate()) {
            $this->autoCreateOfferAction->execute(new ProductData(
                id: $product->id,
                allowPublish: $product->allow_publish,
            ));
        }

        if ($eventMessage->isUpdate() && $eventMessage->isDirty('allow_publish')) {
            $context = new CalculationOfferContext();
            $context->productId = $product->id;
            $context->productAllowPublish = $product->allow_publish;

            $this->calculationOfferActivityAction->execute($context);
        }

        if ($eventMessage->isDelete()) {
            $context = new CalculationOfferContext();
            $context->productId = $product->id;
            $context->productAllowPublish = false;

            $this->calculationOfferActivityAction->execute($context);
        }
    }
}
