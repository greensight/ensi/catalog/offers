<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Store\StoreEventMessage;
use App\Domain\Stocks\Actions\Stocks\AutoCreateBaseStocksByStoreAction;
use App\Domain\Stocks\Actions\Stocks\ChangeActiveStocksAction;
use App\Domain\Stocks\Actions\Stocks\Data\StoreData;
use RdKafka\Message;

class ListenStoreAction
{
    public function __construct(
        protected readonly AutoCreateBaseStocksByStoreAction $autoCreateBaseStocksByStoreAction,
        protected readonly ChangeActiveStocksAction $changeActiveStocksAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = StoreEventMessage::makeFromRdKafka($message);
        $store = $eventMessage->attributes;

        if ($eventMessage->isCreate()) {
            $this->autoCreateBaseStocksByStoreAction->execute(new StoreData(
                id: $store->id,
                isActive: $store->active,
            ));
        }

        if ($eventMessage->isUpdate() && $eventMessage->isDirty('active')) {
            $this->changeActiveStocksAction->execute(
                isActive: $store->active,
                storeId: $store->id
            );
        }
    }
}
