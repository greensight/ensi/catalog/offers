<?php

use App\Domain\Common\Tests\Factories\Units\StoreFactory;
use App\Domain\Kafka\Actions\Listen\ListenPublishedProductAction;
use App\Domain\Kafka\Actions\Listen\ListenStoreAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct\PublishedProductEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Store\StoreEventMessage;
use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Models\Stock;
use Ensi\BuClient\Dto\ResponseBodyPagination;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit', 'kafka');

test("Action ListenPublishedProductAction create success", function () {
    /** @var IntegrationTestCase $this */
    $productId = 1;
    $storeId1 = 2;
    $storeId2 = 3;
    $message = PublishedProductEventMessage::factory()->make([
        'event' => ModelEventMessage::CREATE,
        'attributes' => [
            'id' => $productId,
            'allow_publish' => true,
        ],
    ]);

    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('searchStores')
        ->andReturn(
            StoreFactory::new()->makeResponseSearch(
                extras: [['id' => $storeId1], ['id' => $storeId2]]
            ),
            pagination: PaginationFactory::new()->makeResponseCursor(ResponseBodyPagination::class),
        );

    resolve(ListenPublishedProductAction::class)->execute($message);

    $offers = Offer::query()->where('product_id', $productId)->get();
    expect($offers->count())->toBe(1);

    /** @var Offer $offer */
    $offer = $offers->first();

    expect($offer->allow_publish)->toBe(false)
        ->and($offer->is_active)->toBe(false);


    assertDatabaseHas(Stock::class, [
        'offer_id' => $offer->id,
        'store_id' => $storeId1,
        'qty' => null,
    ]);
    assertDatabaseHas(Stock::class, [
        'offer_id' => $offer->id,
        'store_id' => $storeId2,
        'qty' => null,
    ]);
});

test("Action ListenPublishedProductAction update success", function (bool $isActive) {
    /** @var IntegrationTestCase $this */
    $productId = 1;

    $offerActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => true, 'product_active' => !$isActive]);
    $offer = Offer::factory()
        ->withPrice()
        ->allowPublish()
        ->withActiveData($offerActiveData)
        ->create(['product_id' => $productId]);

    $otherOfferActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => true, 'product_active' => !$isActive]);
    $otherOffer = Offer::factory()
        ->withPrice()
        ->allowPublish()
        ->withActiveData($otherOfferActiveData)
        ->create();

    $message = PublishedProductEventMessage::factory()->make([
        'event' => ModelEventMessage::UPDATE,
        'attributes' => [
            'id' => $productId,
            'allow_publish' => $isActive,
        ],
        'dirty' => ['allow_publish'],
    ]);

    resolve(ListenPublishedProductAction::class)->execute($message);

    assertDatabaseHas(Offer::class, [
        'id' => $offer->id,
        'is_active' => $isActive,
    ]);

    assertDatabaseHas(Offer::class, [
        'id' => $otherOffer->id,
        'is_active' => !$isActive,
    ]);

})->with([true, false]);

test("Action ListenPublishedProductAction delete success", function () {
    /** @var IntegrationTestCase $this */
    $productId = 1;

    $offerActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => true, 'product_active' => true]);
    $offer = Offer::factory()
        ->withPrice()
        ->allowPublish()
        ->withActiveData($offerActiveData)
        ->create(['product_id' => $productId]);

    $otherOfferActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => true, 'product_active' => true]);
    $otherOffer = Offer::factory()
        ->withPrice()
        ->allowPublish()
        ->withActiveData($otherOfferActiveData)
        ->create();

    $message = PublishedProductEventMessage::factory()->make([
        'event' => ModelEventMessage::DELETE,
        'attributes' => [
            'id' => $productId,
        ],
    ]);

    resolve(ListenPublishedProductAction::class)->execute($message);

    assertDatabaseHas(Offer::class, [
        'id' => $offer->id,
        'is_active' => false,
    ]);

    assertDatabaseHas(Offer::class, [
        'id' => $otherOffer->id,
        'is_active' => true,
    ]);
});

test("Action ListenStoreAction create success", function () {
    /** @var IntegrationTestCase $this */
    $storeId = 1;

    $offer1 = Offer::factory()->createQuietly();
    $offer2 = Offer::factory()->createQuietly();

    $message = StoreEventMessage::factory()->make([
        'event' => ModelEventMessage::CREATE,
        'attributes' => [
            'id' => $storeId,
            'active' => true,
        ],
    ]);

    Offer::factory()->createQuietly();
    Offer::factory()->createQuietly();

    resolve(ListenStoreAction::class)->execute($message);

    assertDatabaseHas(Stock::class, [
        'store_id' => $storeId,
        'offer_id' => $offer1->id,
        'qty' => null,
    ]);
    assertDatabaseHas(Stock::class, [
        'store_id' => $storeId,
        'offer_id' => $offer2->id,
        'qty' => null,
    ]);
});

test("Action ListenStoreAction update success", function (bool $isActive) {
    /** @var IntegrationTestCase $this */
    $storeId = 1;

    $message = StoreEventMessage::factory()->make([
        'event' => ModelEventMessage::UPDATE,
        'attributes' => [
            'id' => $storeId,
            'active' => $isActive,
        ],
        'dirty' => ['active'],
    ]);

    $stock = Stock::factory()->withQty()->create(['is_active' => !$isActive]);
    $storeStock = Stock::factory()->withQty()->create(['store_id' => $storeId, 'is_active' => !$isActive]);

    resolve(ListenStoreAction::class)->execute($message);

    assertDatabaseHas(Stock::class, [
        'id' => $stock->id,
        'is_active' => !$isActive,
    ]);
    assertDatabaseHas(Stock::class, [
        'id' => $storeStock->id,
        'is_active' => $isActive,
    ]);
})->with([true, false]);
