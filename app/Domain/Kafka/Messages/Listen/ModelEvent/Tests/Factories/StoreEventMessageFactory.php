<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

class StoreEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'active' => $this->faker->boolean(),
        ];
    }
}
