<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

class PublishedProductEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'allow_publish' => $this->faker->boolean(),
        ];
    }
}
