<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent;

use RdKafka\Message;

abstract class ModelEventMessage
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    public function __construct(
        public ?array $dirty,
        array $attributes,
        public string $event,
    ) {
        $this->setAttributes($attributes);
    }

    abstract public function setAttributes(array $attributes): void;

    public static function makeFromRdKafka(Message $message): static
    {
        $payload = json_decode($message->payload, true);

        return new static(
            $payload['dirty'],
            $payload['attributes'],
            $payload['event'],
        );
    }

    public function isCreate(): bool
    {
        return $this->event === self::CREATE;
    }

    public function isUpdate(): bool
    {
        return $this->event === self::UPDATE;
    }

    public function isDelete(): bool
    {
        return $this->event === self::DELETE;
    }

    public function isDirty(string $key): bool
    {
        return in_array($key, $this->dirty ?? []);
    }
}
