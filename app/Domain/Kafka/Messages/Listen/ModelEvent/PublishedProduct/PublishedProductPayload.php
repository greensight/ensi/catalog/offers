<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;

/**
 * @property int $id
 * @property bool $allow_publish Признак активности для витрины
 */
class PublishedProductPayload extends Payload
{
}
