<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Store;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\StoreEventMessageFactory;

class StoreEventMessage extends ModelEventMessage
{
    public StorePayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new StorePayload($attributes);
    }

    public static function factory(): StoreEventMessageFactory
    {
        return StoreEventMessageFactory::new();
    }
}
