<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Store;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;

/**
 * @property int $id
 * @property bool $active
 */
class StorePayload extends Payload
{
}
