<?php

use App\Domain\Kafka\Messages\Send\ModelEvent\OfferPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\StockPayload;
use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Models\Stock;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-send-messages');

test("generate OfferPayload success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Offer $model */
    $model = Offer::factory()->create();

    $payload = new OfferPayload($model);

    assertEquals($payload->jsonSerialize(), array_merge($model->attributesToArray(), ['is_real_active' => $model->isRealActive()]));
})->with(FakerProvider::$optionalDataset);

test("generate StockPayload success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = Stock::factory()->create();

    $payload = new StockPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
})->with(FakerProvider::$optionalDataset);
