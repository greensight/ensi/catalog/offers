<?php

namespace App\Domain\Kafka\Messages\Send;

use JsonSerializable;

abstract class Payload implements JsonSerializable
{
}
