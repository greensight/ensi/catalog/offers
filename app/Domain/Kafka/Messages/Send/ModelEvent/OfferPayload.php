<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Offers\Models\Offer;

class OfferPayload extends Payload
{
    public function __construct(protected Offer $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,
            'product_id' => $this->model->product_id,
            'price' => $this->model->price,
            'allow_publish' => $this->model->allow_publish,

            'is_active' => $this->model->is_active,
            'is_real_active' => $this->model->isRealActive(),

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
