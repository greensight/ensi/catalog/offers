<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Stocks\Models\Stock;

class StockPayload extends Payload
{
    public function __construct(protected Stock $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'store_id' => $this->model->store_id,
            'offer_id' => $this->model->offer_id,
            'qty' => $this->model->qty,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
