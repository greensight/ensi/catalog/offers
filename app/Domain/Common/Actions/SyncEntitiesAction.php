<?php

namespace App\Domain\Common\Actions;

use App\Domain\Offers\Actions\Offers\RefreshOffersDataAction;
use App\Domain\Stocks\Actions\Stocks\RefreshStocksDataAction;
use App\Exceptions\ExceptionFormatter;
use Psr\Log\LoggerInterface;
use Throwable;

class SyncEntitiesAction
{
    protected LoggerInterface $logger;

    public function __construct(
        protected readonly RefreshStocksDataAction $refreshStocksAction,
        protected readonly RefreshOffersDataAction $refreshOffersAction,
    ) {
        $this->logger = logger()->channel('entities:sync');
    }

    public function execute(): void
    {
        try {
            $this->logger->info("=== Начато обновление данных");
            $this->refreshStocksAction->execute();
            $this->logger->info("Обновились данные по стокам");
            $this->refreshOffersAction->execute();
            $this->logger->info("Обновились данные по офферам");

        } catch (Throwable $e) {
            $this->logger->error(ExceptionFormatter::line("Ошибка при обновлении данных", $e));
        }
        $this->logger->info("=== Закончено обновление данных");
    }
}
