<?php

namespace App\Domain\Common\Tests;

use App\Domain\Common\Jobs\SyncEntitiesJob;
use App\Domain\Common\Tests\Factories\Pim\ProductFactory;
use App\Domain\Common\Tests\Factories\Units\StoreFactory;
use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Models\Stock;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

uses(ApiV1ComponentTestCase::class)->group('component');

test('Job SyncEntitiesJob success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $offerActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => false, 'product_active' => false]);
    /** @var Offer $offer */
    $offer = Offer::factory()->withPrice()->withActiveData($offerActiveData)->create();
    /** @var Stock $stock */
    $stock = Stock::factory()->withQty()->for($offer)->createQuietly(['is_active' => false]);

    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('searchStores')
        ->andReturn(
            StoreFactory::new()->makeResponseSearch(
                extras: [['id' => $stock->store_id, 'active' => true]]
            ),
        );

    $this->mockPimProductsApi()
        ->shouldReceive('searchProducts')
        ->andReturn(
            ProductFactory::new()->makeResponseSearch(
                extras: [['id' => $offer->product_id, 'allow_publish' => true]]
            ),
        );

    SyncEntitiesJob::dispatchSync();

    $offer->refresh();
    $stock->refresh();

    expect($stock->is_active)->toBe(true)
        ->and($offer->active_data->product_active)->toBe(true)
        ->and($offer->active_data->stocks_exists_active)->toBe(true);
});
