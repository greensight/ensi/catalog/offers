<?php

namespace App\Domain\Common\Tests\Factories\Units;

use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\Store;
use Ensi\BuClient\Dto\StoreResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'xml_id' => $this->faker->nullable()->word(),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => $this->faker->nullable()->exactly(AddressFactory::new()->make()),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Store
    {
        return new Store($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): StoreResponse
    {
        return new StoreResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStoresResponse
    {
        return $this->generateResponseSearch(SearchStoresResponse::class, $extras, $count, $pagination);
    }
}
