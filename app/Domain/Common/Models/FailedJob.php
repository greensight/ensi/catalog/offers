<?php

namespace App\Domain\Common\Models;

use App\Domain\Common\Models\Tests\Factories\FailedJobFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $uuid - Job uuid
 * @property string $connection
 * @property string $queue
 * @property string $payload - Full message payload
 * @property string $exception - Exception (with stack trace)
 * @property CarbonInterface $failed_at
 */
class FailedJob extends Model
{
    protected $table = 'failed_jobs';

    public $timestamps = false;

    protected $casts = [
        'failed_at' => 'datetime',
    ];

    public static function factory(): FailedJobFactory
    {
        return FailedJobFactory::new();
    }
}
