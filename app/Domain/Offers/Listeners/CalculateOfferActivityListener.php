<?php

namespace App\Domain\Offers\Listeners;

use App\Domain\Offers\Actions\Offers\CalculationOffersActivityAction;
use App\Domain\Offers\Events\CalculateOfferActivity;

class CalculateOfferActivityListener
{
    public function __construct(protected readonly CalculationOffersActivityAction $calculationOffersActivityAction)
    {
    }

    public function handle(CalculateOfferActivity $event): void
    {
        $this->calculationOffersActivityAction->execute($event->context);
    }
}
