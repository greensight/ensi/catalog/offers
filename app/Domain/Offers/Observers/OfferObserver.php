<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Models\Offer;

class OfferObserver
{
    public function saving(Offer $offer): void
    {
        $offer->setActive();
    }
}
