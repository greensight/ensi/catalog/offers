<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Kafka\Actions\Send\SendOfferEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Models\Offer;

class OfferAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(protected readonly SendOfferEventAction $sendOfferEventAction)
    {
    }

    public function created(Offer $offer): void
    {
        $this->sendOfferEventAction->execute($offer, ModelEventMessage::CREATE);
    }

    public function updated(Offer $offer): void
    {
        $this->sendOfferEventAction->execute($offer, ModelEventMessage::UPDATE);
    }
}
