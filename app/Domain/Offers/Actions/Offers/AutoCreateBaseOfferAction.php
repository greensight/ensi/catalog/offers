<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Offers\Actions\Offers\Data\ProductData;
use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Actions\Stocks\AutoCreateBaseStocksByOfferAction;
use Illuminate\Support\Facades\DB;

class AutoCreateBaseOfferAction
{
    public function __construct(
        protected readonly AutoCreateBaseStocksByOfferAction $autoCreateBaseStocksByOfferAction,
    ) {
    }

    public function execute(ProductData $productData): void
    {
        /** @var Offer|null $offer */
        $offer = Offer::query()->where('product_id', $productData->id)->first();

        DB::transaction(function () use ($productData, $offer) {
            if (!$offer) {
                $offer = new Offer();
                $offer->product_id = $productData->id;
                $offer->allow_publish = false;

                $offer->active_data = new OfferActiveData();
                $offer->active_data->stocks_exists_active = false;
                $offer->active_data->product_active = $productData->allowPublish;

                $offer->save();
            }

            $this->autoCreateBaseStocksByOfferAction->execute($offer->id);
        });
    }
}
