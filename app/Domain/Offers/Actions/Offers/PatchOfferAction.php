<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Offers\Models\Offer;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class PatchOfferAction
{
    use AppliesToAggregate;

    public function execute(int $offerId, array $fields): Offer
    {
        return $this->updateOrCreate($offerId, function (Offer $offer) use ($fields) {
            $offer->fill($fields);
        });
    }

    protected function createModel(): Model
    {
        return new Offer();
    }
}
