<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Offer;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RefreshOffersDataAction
{
    public function __construct(protected readonly ProductsApi $productsApi)
    {
    }

    public function execute(): void
    {
        DB::transaction(function () {
            Offer::query()
                ->with('realStocks')
                ->chunkById(200, function (Collection $offers) {
                    $this->refreshActive($offers);
                });
        });
    }

    protected function refreshActive(Collection $offers): void
    {
        $productIds = $offers->pluck('product_id')->unique()->all();

        $products = $this->loadProducts($productIds);

        /** @var Offer $offer */
        foreach ($offers as $offer) {
            /** @var Product|null $product */
            $product = $products->get($offer->product_id);

            $offer->active_data = $offer->active_data ?? new OfferActiveData();
            $offer->active_data->product_active = $product?->getAllowPublish() ?? false;
            $offer->active_data->stocks_exists_active = $offer->realStocks->count() > 0;

            $offer->save();
        }
    }

    protected function loadProducts(array $productIds): Collection
    {
        $request = new SearchProductsRequest();
        $request->setFilter((object)['id' => $productIds]);
        $request->setPagination((new RequestBodyPagination())->setLimit(count($productIds))->setType(PaginationTypeEnum::CURSOR));

        return collect($this->productsApi->searchProducts($request)->getData())->keyBy('id');
    }
}
