<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Offers\Actions\Offers\Data\CalculationOfferContext;
use App\Domain\Offers\Models\Offer;
use Illuminate\Support\Collection;

class CalculationOffersActivityAction
{
    public function execute(CalculationOfferContext $context): void
    {
        $offers = $this->loadOffers($context);

        foreach ($offers as $offer) {
            $this->fillActiveSigns($offer, $context);
            $offer->save();
        }
    }

    protected function fillActiveSigns(Offer $offer, CalculationOfferContext $context): void
    {
        if ($context->productAllowPublish !== null) {
            $offer->active_data->product_active = $context->productAllowPublish;
        }
        if ($context->isUpdateStock) {
            $offer->active_data->stocks_exists_active = $offer->realStocks->count() > 0;
        }

    }

    /** @return Collection<Offer> */
    protected function loadOffers(CalculationOfferContext $context): Collection
    {
        $offerQuery = Offer::query();

        if ($context->isUpdateStock) {
            $offerQuery->with('realStocks');
        }

        if ($context->offerIds) {
            $offerQuery->whereIn('id', $context->offerIds);
        }
        if ($context->productId) {
            $offerQuery->where('product_id', $context->productId);
        }

        return $offerQuery->get();
    }
}
