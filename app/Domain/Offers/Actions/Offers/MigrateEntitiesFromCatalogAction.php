<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Models\Stock;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\RequestBodyPagination as BuRequestBodyPagination;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PaginationTypeEnum as BuPaginationTypeEnum;
use Ensi\PimClient\Dto\PaginationTypeEnum as PimPaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination as PimRequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Arr;
use Psr\Log\LoggerInterface;

class MigrateEntitiesFromCatalogAction
{
    protected LoggerInterface $logger;
    protected array $offerIds = [];
    protected array $stockIds = [];

    public function __construct(
        protected readonly ProductsApi $productsApi,
        protected readonly StoresApi $storesApi,
    ) {
        $this->logger = logger()->channel('offers:migration');
    }

    public function execute(): void
    {
        $this->logger->info('Запуск синхронизации состояния офферов относительно PIM и складов');
        $this->loadProducts();
        $this->logger->info("Обновились состояния офферов");
        $this->loadStores();
        $this->logger->info("Обновились состояния стоков");
    }

    protected function loadProducts(): void
    {
        $request = new SearchProductsRequest();
        $request->setPagination((new PimRequestBodyPagination())->setLimit(50)->setType(PimPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->productsApi->searchProducts($request);

            $productIds = Arr::pluck($response->getData(), 'id');
            $offers = Offer::query()->whereIn('product_id', $productIds)->get()->keyBy('product_id');
            foreach ($response->getData() as $productPim) {
                /** @var Offer|null $offer */
                $offer = $offers->get($productPim->getId());
                if ($offer) {
                    $this->offerIds[] = $offer->id;

                    continue;
                }

                $offer = new Offer();
                $offer->product_id = $productPim->getId();
                $offer->allow_publish = false;
                $offer->active_data = new OfferActiveData();
                $offer->active_data->stocks_exists_active = false;
                $offer->active_data->product_active = $productPim->getAllowPublish();

                $offer->save();
                $this->offerIds[] = $offer->id;
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->logger->info('Получена информация о ' . count($this->offerIds) . ' офферах');
        Offer::query()->whereNotIn('id', $this->offerIds)->delete();
    }

    protected function loadStores(): void
    {
        $request = new SearchStoresRequest();
        $request->setPagination((new BuRequestBodyPagination())->setLimit(50)->setType(BuPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->storesApi->searchStores($request);

            foreach ($response->getData() as $storeBu) {
                $stocks = Stock::query()
                    ->where('store_id', $storeBu->getId())
                    ->get()
                    ->keyBy('offer_id');
                foreach ($this->offerIds as $offerId) {
                    /** @var Stock|null $stock */
                    $stock = $stocks->get($offerId);
                    if ($stock) {
                        $this->stockIds[] = $stock->id;

                        continue;
                    }

                    $stock = new Stock();
                    $stock->store_id = $storeBu->getId();
                    $stock->offer_id = $offerId;
                    $stock->is_active = $storeBu->getActive();

                    $stock->save();
                    $this->stockIds[] = $stock->id;
                }
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->logger->info('Получена информация о ' . count($this->stockIds) . ' стоках');
        Stock::query()->whereNotIn('id', $this->stockIds)->delete();
        $this->stockIds = [];
    }
}
