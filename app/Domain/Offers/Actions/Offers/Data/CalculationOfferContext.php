<?php

namespace App\Domain\Offers\Actions\Offers\Data;

class CalculationOfferContext
{
    // фильтрация
    public ?int $productId = null;
    public ?array $offerIds = null;

    // контекст
    /** Новая активность продукта, если null то изменения не было */
    public ?bool $productAllowPublish = null;
    /** Был ли изменен сток  */
    public bool $isUpdateStock = false;
}
