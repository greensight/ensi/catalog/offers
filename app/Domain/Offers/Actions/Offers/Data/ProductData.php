<?php

namespace App\Domain\Offers\Actions\Offers\Data;

class ProductData
{
    public function __construct(
        public readonly int $id,
        public readonly bool $allowPublish
    ) {
    }
}
