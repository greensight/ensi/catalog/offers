<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Offer;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Offer>
 */
class OfferFactory extends BaseModelFactory
{
    protected $model = Offer::class;

    public function definition(): array
    {
        return [
            'product_id' => $this->faker->unique()->modelId(),
            'price' => $this->faker->nullable()->numberBetween(10, 100_000),
            'allow_publish' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
            'active_data' => OfferActiveData::factory()->make(),
        ];
    }

    public function withPrice(): static
    {
        return $this->state(['price' => $this->faker->numberBetween(10, 100_000)]);
    }

    public function withActiveData(?OfferActiveData $offerActiveData = null): static
    {
        return $this->state(['active_data' => $offerActiveData ?? OfferActiveData::factory()->make()]);
    }

    public function allowPublish(bool $flag = true): static
    {
        return $this->state(['allow_publish' => $flag]);
    }
}
