<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Tests\Factories\OfferFactory;
use App\Domain\Stocks\Models\Stock;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $product_id - id товара
 * @property int|null $price - цена
 * @property bool $allow_publish Признак активности для витрины
 *
 * @property bool $is_active - Системная активность оффера, зависит от: активность товара, наличие активного заполненного стока
 * @property OfferActiveData|null $active_data Данные активности
 *
 * @property-read Collection|Stock[] $stocks
 * @property-read Collection|Stock[] $realStocks - действительный сток
 */
class Offer extends Model
{
    protected $table = 'offers';

    protected $fillable = ['allow_publish', 'price'];

    protected $hidden = ['active_data'];

    protected $casts = [
        'product_id' => 'int',
        'is_active' => 'bool',
        'allow_publish' => 'bool',
        'price' => 'int',
    ];

    public static function factory(): OfferFactory
    {
        return OfferFactory::new();
    }

    public function setActive(): void
    {
        if ($this->price <= 0) {
            $this->is_active = false;
            $this->active_data->active_comment = 'Не заполнена цена';

            return;
        }

        if (!$this->active_data->product_active) {
            $this->is_active = false;
            $this->active_data->active_comment = 'Не активен продукт';

            return;
        }

        if (!$this->active_data->stocks_exists_active) {
            $this->is_active = false;
            $this->active_data->active_comment = 'Нет активного стока';

            return;
        };

        $this->is_active = true;
        $this->active_data->active_comment = null;
    }

    protected function activeData(): Attribute
    {
        return Attribute::make(
            get: fn (?string $value) => $value ? new OfferActiveData(json_decode($value)) : null,
            set: fn (OfferActiveData $value) => $value->toJson(),
        );
    }

    public function realStocks(): HasMany
    {
        return $this->hasMany(Stock::class, 'offer_id')
            ->where('qty', '>', 0)
            ->where('is_active', true);
    }

    public function isRealActive(): bool
    {
        return $this->allow_publish && $this->is_active;
    }

    public function stocks(): HasMany
    {
        return $this->hasMany(Stock::class, 'offer_id');
    }
}
