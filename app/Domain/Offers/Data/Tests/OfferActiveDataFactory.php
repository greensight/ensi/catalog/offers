<?php

namespace App\Domain\Offers\Data\Tests;

use App\Domain\Offers\Data\OfferActiveData;
use Ensi\LaravelTestFactories\Factory;

class OfferActiveDataFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'product_active' => $this->faker->boolean(),
            'stocks_exists_active' => $this->faker->boolean(),
            'active_comment' => $this->faker->nullable()->word(),
        ];
    }

    public function make(array $extra = []): OfferActiveData
    {
        return new OfferActiveData($this->makeArray($extra));
    }
}
