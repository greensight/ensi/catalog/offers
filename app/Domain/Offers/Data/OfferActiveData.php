<?php

namespace App\Domain\Offers\Data;

use App\Domain\Offers\Data\Tests\OfferActiveDataFactory;
use Illuminate\Support\Fluent;

/**
 * @property bool $product_active Соответствует активности продукта для витрины
 * @property bool $stocks_exists_active Наличие активного стока с заполненным значением
 *
 * @property string|null $active_comment
 */
class OfferActiveData extends Fluent
{
    public static function factory(): OfferActiveDataFactory
    {
        return OfferActiveDataFactory::new();
    }
}
