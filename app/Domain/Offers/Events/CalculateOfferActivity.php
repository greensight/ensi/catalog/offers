<?php

namespace App\Domain\Offers\Events;

use App\Domain\Offers\Actions\Offers\Data\CalculationOfferContext;
use App\Domain\Stocks\Models\Stock;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;

class CalculateOfferActivity
{
    use Dispatchable;
    use InteractsWithSockets;

    public function __construct(public CalculationOfferContext $context)
    {

    }

    public static function dispatchByOfferIds(array $offerId): void
    {
        static::dispatch(self::getContext($offerId));
    }

    public static function dispatchIfChangedStateStock(Stock $stock): void
    {
        if ($stock->wasChanged(['is_active', 'qty'])) {
            static::dispatch(self::getContext([$stock->offer_id]));
        }
    }

    protected static function getContext(array $offerIds): CalculationOfferContext
    {
        $context = new CalculationOfferContext();
        $context->offerIds = $offerIds;
        $context->isUpdateStock = true;

        return $context;
    }
}
