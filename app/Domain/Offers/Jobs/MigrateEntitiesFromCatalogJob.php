<?php

namespace App\Domain\Offers\Jobs;

use App\Domain\Offers\Actions\Offers\MigrateEntitiesFromCatalogAction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MigrateEntitiesFromCatalogJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function handle(MigrateEntitiesFromCatalogAction $action): void
    {
        $action->execute();
    }
}
