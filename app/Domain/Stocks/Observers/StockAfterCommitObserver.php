<?php

namespace App\Domain\Stocks\Observers;

use App\Domain\Kafka\Actions\Send\SendStockEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Actions\Offers\CalculationOffersActivityAction;
use App\Domain\Stocks\Models\Stock;

class StockAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected readonly SendStockEventAction $sendStockEventAction,
        protected readonly CalculationOffersActivityAction $calculationOfferActiveAction,
    ) {
    }

    public function created(Stock $stock): void
    {
        $this->sendStockEventAction->execute($stock, ModelEventMessage::CREATE);
    }

    public function updated(Stock $stock): void
    {
        $this->sendStockEventAction->execute($stock, ModelEventMessage::UPDATE);
    }
}
