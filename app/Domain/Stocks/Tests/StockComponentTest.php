<?php

use App\Domain\Offers\Data\OfferActiveData;
use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Actions\Stocks\ChangeActiveStocksAction;
use App\Domain\Stocks\Actions\Stocks\PatchStockAction;
use App\Domain\Stocks\Models\Stock;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;

uses(ApiV1ComponentTestCase::class)->group('offers', 'component');

test('Action ChangeActiveStocksAction calculate offer activity where change active', function (bool $stockActive) {
    $offerActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => !$stockActive, 'product_active' => true]);
    /** @var Offer $offer */
    $offer = Offer::factory()
        ->withPrice()
        ->allowPublish()
        ->withActiveData($offerActiveData)
        ->create();

    /** @var Stock $stock */
    $stock = Stock::factory()->withQty()->for($offer)->createQuietly(['is_active' => !$stockActive]);

    $action = resolve(ChangeActiveStocksAction::class);
    $action->execute($stockActive, $stock->store_id);

    assertDatabaseHas(
        Offer::class,
        ['id' => $offer->id, 'is_active' => $stockActive]
    );
})->with([true, false]);

test('Action ChangeActiveStocksAction calculate offer activity witch other active stock', function () {
    $offerActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => true, 'product_active' => true]);
    /** @var Offer $activeOffer */
    $activeOffer = Offer::factory()
        ->withPrice()
        ->allowPublish()
        ->withActiveData($offerActiveData)
        ->create();
    /** @var Stock $activeStock */
    $activeStock = Stock::factory()->withQty()->for($activeOffer)->createQuietly(['is_active' => true]);

    /** @var Stock $deactivateStock */
    $deactivateStock = Stock::factory()->withQty()->for($activeOffer)->createQuietly(['is_active' => true]);

    $action = resolve(ChangeActiveStocksAction::class);
    $action->execute(false, $deactivateStock->store_id);

    assertDatabaseHas(
        Offer::class,
        ['id' => $activeOffer->id, 'is_active' => true]
    );
});

test('Action PatchStockAction calculate offer activity where change qty', function (?float $currentQty, ?float $newQty, bool $assertActive) {
    $offerActiveData = OfferActiveData::factory()->make(['stocks_exists_active' => !$assertActive, 'product_active' => true]);
    /** @var Offer $activeOffer */
    $activeOffer = Offer::factory()
        ->withPrice()
        ->allowPublish()
        ->withActiveData($offerActiveData)
        ->create();

    /** @var Stock $stock */
    $stock = Stock::factory()
        ->for($activeOffer)
        ->createQuietly(['qty' => $currentQty, 'is_active' => true]);

    $action = resolve(PatchStockAction::class);
    $action->execute($stock->id, ['qty' => $newQty]);

    assertDatabaseHas(
        Offer::class,
        ['id' => $activeOffer->id, 'is_active' => $assertActive]
    );
})->with([
    'set qty' => [null, 1, true],
    'delete qty' => [1, null, false],
    'set 0' => [5, 0, false],
    'update qty' => [5, 10, true],
    'reset qty' => [0, null, false],
]);
