<?php

namespace App\Domain\Stocks\Models;

use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Models\Tests\Factories\StockFactory;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Остатки товаров на складах (запасы)"
 *
 * @property int $offer_id - id предложения продавца
 * @property int $store_id - id склада
 * @property float|null $qty - кол-во товара
 * @property bool $is_active - системная активность стока, зависит от: активность магазина
 *
 * @property-read Offer $offer
 */
class Stock extends Model
{
    protected $table = 'stocks';

    protected $fillable = ['qty'];

    protected $hidden = ['is_active'];

    protected $casts = [
        'offer_id' => 'int',
        'store_id' => 'int',
        'qty' => 'float',
        'is_active' => 'bool',
    ];

    public static function factory(): StockFactory
    {
        return StockFactory::new();
    }

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }
}
