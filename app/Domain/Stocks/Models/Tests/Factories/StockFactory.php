<?php

namespace App\Domain\Stocks\Models\Tests\Factories;

use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Models\Stock;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Stock>
 */
class StockFactory extends BaseModelFactory
{
    protected $model = Stock::class;

    public function definition(): array
    {
        return [
            'store_id' => $this->faker->unique()->modelId(),
            'offer_id' => Offer::factory(),
            'is_active' => $this->faker->boolean(),
            'qty' => $this->faker->nullable()->randomFloat(0, 1, 99),
        ];
    }

    public function withQty(): static
    {
        return $this->state(['qty' => $this->faker->randomFloat(0, 1, 99)]);
    }
}
