<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Offers\Events\CalculateOfferActivity;
use App\Domain\Stocks\Models\Stock;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ChangeActiveStocksAction
{
    public function execute(bool $isActive, ?int $storeId): void
    {
        $stocks = $this->loadStock($isActive, $storeId);

        DB::transaction(function () use ($stocks, $isActive) {
            foreach ($stocks as $stock) {
                $stock->is_active = $isActive;
                $stock->save();
            }
        });

        $offerIds = $stocks->pluck('offer_id')->unique()->all();

        CalculateOfferActivity::dispatchByOfferIds($offerIds);
    }

    /** @return Collection<Stock> */
    protected function loadStock(bool $isActive, ?int $storeId): Collection
    {
        $stockQuery = Stock::query()->where('is_active', '!=', $isActive);

        if ($storeId) {
            $stockQuery->where('store_id', '=', $storeId);
        }

        return $stockQuery->get();
    }
}
