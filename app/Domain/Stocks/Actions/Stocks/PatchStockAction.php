<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Offers\Events\CalculateOfferActivity;
use App\Domain\Stocks\Models\Stock;

class PatchStockAction
{
    public function execute(int $id, array $fields): Stock
    {
        /** @var Stock $stock */
        $stock = Stock::query()->findOrFail($id);
        $stock->update($fields);

        CalculateOfferActivity::dispatchIfChangedStateStock($stock);

        return $stock;
    }
}
