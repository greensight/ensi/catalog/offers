<?php

namespace App\Domain\Stocks\Actions\Stocks\Data;

class StoreData
{
    public function __construct(
        public readonly int $id,
        public readonly bool $isActive,
    ) {
    }
}
