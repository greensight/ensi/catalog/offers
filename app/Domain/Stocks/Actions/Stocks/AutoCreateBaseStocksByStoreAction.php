<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Actions\Stocks\Data\StoreData;
use App\Domain\Stocks\Models\Stock;
use Illuminate\Support\Facades\DB;
use Traversable;

class AutoCreateBaseStocksByStoreAction
{
    public function execute(StoreData $storeData): void
    {
        $offers = $this->getOffers();

        DB::transaction(function () use ($storeData, $offers) {
            foreach ($offers as $offer) {
                $this->createStock(offerId: $offer->id, storeId: $storeData->id, isActive: $storeData->isActive);
            }
        });
    }

    protected function createStock(int $offerId, int $storeId, bool $isActive): void
    {
        $stock = new Stock();
        $stock->store_id = $storeId;
        $stock->offer_id = $offerId;
        $stock->is_active = $isActive;

        $stock->save();
    }

    protected function getOffers(): Traversable
    {
        return Offer::query()->select('id')->lazyById();
    }
}
