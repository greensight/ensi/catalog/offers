<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Stocks\Models\Stock;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\PaginationTypeEnum;
use Ensi\BuClient\Dto\RequestBodyPagination;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\BuClient\Dto\Store;
use Illuminate\Support\Facades\DB;
use Traversable;

class AutoCreateBaseStocksByOfferAction
{
    public function __construct(protected readonly StoresApi $storesApi)
    {
    }

    public function execute(int $offerId): void
    {
        /** @var Store[] $stores */
        $stores = $this->getStores();
        $stocks = Stock::query()->where('offer_id', $offerId)->get()->keyBy('store_id');

        DB::transaction(function () use ($offerId, $stores, $stocks) {
            foreach ($stores as $store) {
                if (!$stocks->has($store->getId())) {
                    $this->createStock(offerId: $offerId, storeId: $store->getId(), isActive: $store->getActive());
                }
            }
        });
    }

    protected function createStock(int $offerId, int $storeId, bool $isActive): void
    {
        $stock = new Stock();
        $stock->store_id = $storeId;
        $stock->offer_id = $offerId;
        $stock->is_active = $isActive;

        $stock->save();
    }

    protected function getStores(): Traversable
    {
        $request = new SearchStoresRequest();
        $request->setPagination((new RequestBodyPagination())->setLimit(50)->setType(PaginationTypeEnum::CURSOR));

        while (true) {
            $response = $this->storesApi->searchStores($request);

            foreach ($response->getData() as $store) {
                yield $store;
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }
    }
}
