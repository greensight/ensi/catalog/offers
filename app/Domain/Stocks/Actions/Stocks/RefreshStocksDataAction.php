<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Stocks\Models\Stock;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\PaginationTypeEnum;
use Ensi\BuClient\Dto\RequestBodyPagination;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\BuClient\Dto\Store;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RefreshStocksDataAction
{
    public function __construct(protected readonly StoresApi $storesApi)
    {
    }

    public function execute(): void
    {
        DB::transaction(function () {
            Stock::query()->chunkById(200, function (Collection $stocks) {
                $this->refreshActive($stocks);
            });
        });
    }

    protected function refreshActive(Collection $stocks): void
    {
        $storeIds = $stocks->pluck('store_id')->unique()->all();

        $stores = $this->loadStores($storeIds);
        /** @var Stock $stock */
        foreach ($stocks as $stock) {
            /** @var Store|null $store */
            $store = $stores->get($stock->store_id);

            $stock->is_active = $store?->getActive() ?? false;
            $stock->save();
        }
    }

    protected function loadStores(array $storeIds): Collection
    {
        $request = new SearchStoresRequest();
        $request->setFilter((object)['id' => $storeIds]);
        $request->setPagination((new RequestBodyPagination())->setLimit(count($storeIds))->setType(PaginationTypeEnum::CURSOR));

        return collect($this->storesApi->searchStores($request)->getData())->keyBy('id');
    }
}
