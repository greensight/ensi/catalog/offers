<?php

namespace App\Console\Commands\Offers;

use App\Domain\Offers\Actions\Offers\MigrateEntitiesFromCatalogAction;
use Illuminate\Console\Command;

class MigrateFromApiCommand extends Command
{
    protected $signature = 'offers:migrate-from-api';

    public function handle(MigrateEntitiesFromCatalogAction $action): void
    {
        $action->execute();
    }
}
