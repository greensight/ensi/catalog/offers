<?php

namespace App\Providers;

use App\Domain\Offers\Events\CalculateOfferActivity;
use App\Domain\Offers\Listeners\CalculateOfferActivityListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        CalculateOfferActivity::class => [
            CalculateOfferActivityListener::class,
        ],
    ];

    public function boot(): void
    {
        //
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
