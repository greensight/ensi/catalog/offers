<?php

namespace App\Providers;

use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Observers\OfferAfterCommitObserver;
use App\Domain\Offers\Observers\OfferObserver;
use App\Domain\Stocks\Models\Stock;
use App\Domain\Stocks\Observers\StockAfterCommitObserver;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\DateFactory;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\ServiceProvider;
use Spatie\QueryBuilder\QueryBuilderRequest;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        DateFactory::useClass(CarbonImmutable::class);
    }

    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Date::use(CarbonImmutable::class);

        QueryBuilderRequest::setFilterArrayValueDelimiter('');

        $this->addObservers();
    }

    protected function addObservers(): void
    {
        Offer::observe(OfferObserver::class);
        Offer::observe(OfferAfterCommitObserver::class);
        Stock::observe(StockAfterCommitObserver::class);
    }
}
