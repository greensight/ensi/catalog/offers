<?php

use App\Domain\Offers\Models\Offer;
use App\Domain\Stocks\Models\Stock;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Offer::factory()->count(100)->create();
        Stock::factory()->count(100)->create();
    }
}
