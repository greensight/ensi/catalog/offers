<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        // Сначала удаляем все дублирующиеся записи
        DB::delete("DELETE FROM offers T1 USING offers T2 WHERE
            T1.product_id = T2.product_id AND
            (
                T1.updated_at > T2.updated_at OR
                (T1.updated_at = T2.updated_at AND T1.id > T2.id)
            )
        ");
        Schema::table('offers', function (Blueprint $table) {
            $table->unique('product_id');
        });

        // Удаляем несуществующие связи с offers
        DB::delete("DELETE FROM stocks WHERE offer_id NOT IN (SELECT id FROM offers)");

        Schema::table('stocks', function (Blueprint $table) {
            $table->foreign('offer_id')->on('offers')->references('id');
        });
    }

    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropUnique(['product_id']);
        });
        Schema::table('stocks', function (Blueprint $table) {
            $table->dropForeign(['offer_id']);
        });
    }
};
