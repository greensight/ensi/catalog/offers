<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public function up()
    {
        // $table->string()->change() не сработало
        DB::statement('alter table offers alter column storage_address type varchar(255) using storage_address::varchar(255)');
    }

    public function down()
    {
    }
};
