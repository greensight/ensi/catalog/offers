<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    protected const DEFAULT_SELLER_ID = 1;

    public function up(): void
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('seller_id');
            $table->dropColumn('external_id');
            $table->dropColumn('sale_status');
            $table->dropColumn('storage_address');

            $table->renameColumn('base_price', 'price');

            $table->boolean('allow_publish')->default(true);
            $table->boolean('is_active')->default(true);
            $table->jsonb('active_data')->nullable(true);
        });

        Schema::table('stocks', function (Blueprint $table) {
            $table->dropColumn('product_id');
            $table->boolean('is_active')->default(true);
            $table->decimal('qty', 18, 4)->nullable()->change();
        });

        Schema::dropIfExists('offer_certs');
    }

    public function down(): void
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('allow_publish');
            $table->dropColumn('is_active');
            $table->dropColumn('active_data');

            $table->renameColumn('price', 'base_price');

            $table->integer('seller_id')->unsigned()->nullable();
            $table->string('external_id')->nullable();
            $table->tinyInteger('sale_status')->unsigned()->default(1);
            $table->string('storage_address')->nullable();

            $table->unique(['product_id', 'seller_id']);
        });

        Schema::table('stocks', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id')->nullable();
            $table->dropColumn('is_active');
        });

        DB::table('offers')->update(['seller_id' => self::DEFAULT_SELLER_ID]);

        Schema::table('offers', function (Blueprint $table) {
            $table->integer('seller_id')->nullable(false)->change();
        });

        DB::statement('UPDATE stocks SET product_id = (SELECT offers.product_id FROM offers WHERE stocks.offer_id = offers.id) WHERE 1=1');
        DB::statement('UPDATE stocks SET qty = 0 WHERE qty is null');

        Schema::table('stocks', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id')->nullable(false)->change();
            $table->decimal('qty', 18, 4)->nullable(false)->change();
        });

        Schema::create('offer_certs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('offer_id')->unsigned();
            $table->string('name')->nullable();
            $table->date('date_end')->nullable();
            $table->jsonb('file')->nullable();

            $table->timestamps();

            $table->foreign('offer_id')->references('id')->on('offers');
        });
    }
};
