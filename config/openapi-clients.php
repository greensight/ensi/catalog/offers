<?php

return [
    'catalog' => [
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'units' => [
        'bu' => [
            'base_uri' => env('UNITS_BU_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
