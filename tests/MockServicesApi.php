<?php

namespace Tests;

use Ensi\BuClient\Api\StoresApi;
use Ensi\PimClient\Api\ProductsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // region service Business Units
    protected function mockBusinessUnitsStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class);
    }
    // endregion

    // region service PIM
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }
    // endregion
}
